<?php
    require_once __DIR__ . "/../config/db.php";

    class Crud extends Database {
        
        // INSERT DATA
        public function store($table, $arr) {
            try {
                $bind = $this->arrBindKey($arr);
                $SQL = "INSERT INTO {$table} 
                    (" . implode(', ', array_keys($arr)) . ") 
                        VALUES 
                    (:" . implode(", :", array_keys($arr)) . ")"; 
                
                $stmt = $this->db->prepare($SQL);
                $store_operation = $stmt->execute($bind);
    
                return $store_operation;
                
            } catch (PDOException $err) {
                echo $err->getMessage();
            }
        }

        // UPDATE DATA
        public function update($table, $arr, $condition) {
            try {
                $data   = $this->strKeyBind($arr);
                $clause = $this->strKeyBind($condition, ' AND ');
                $params = array_merge($arr, $condition);
                $bind   = $this->arrBindKey($params);
                
                $SQL    = "UPDATE {$table} SET {$data} WHERE {$clause}";
                $stmt   = $this->db->prepare($SQL);

                $update_operation = $stmt->execute($bind);

                return $update_operation;

            } catch (PDOException $err) {
                echo $err->getMessage();
            }
        }

        // DELETE DATA
        public function delete($table, $condition) {
            try {
                $bind   = $this->arrBindKey($condition);
                $clause = $this->strKeyBind($condition, ' AND '); 
                $SQL    = "DELETE FROM {$table} WHERE {$clause}";

                $stmt   = $this->db->prepare($SQL); 

                $delete_operation = $stmt->execute($bind);

                return $delete_operation;
            } catch (PDOException $err) {
                echo $err->getMessage();
            }
        }

        // MAKE REPORT
        public function report($user_id, $info) {
            try {
                $this->store('report', [
                    'user_id'       => $user_id,
                    'information'   => $info, 
                    'date'          => date('Ymd'),
                ]);
            } catch (PDOException $err) {
                echo $err->getMessage();
            }
        }

        // MAKE KEY BIND STR => ASSOC TO STR "$key = :$key"
        private function strKeyBind($arr, $separator=',') {
            $str = "";
            foreach ($arr as $k => $v)
                $str .= "{$k}=:{$k}{$separator}";
   
            $str = substr($str, 0 , -strlen($separator));
            return $str; 
        }

        // BIND THE KEY ASSOC => MODIFY FROM ['$key' = '$value'] TO [':$key' = '$value']
        private function arrBindKey($arr) {
            $binded = [];
            foreach ($arr as $k => $v)
                $binded[":{$k}"] = $v;
            
            return $binded;
        } 
    }
?>