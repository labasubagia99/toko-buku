<?php
    // GET BOOKS
    $sql_get_books = "SELECT * FROM book ORDER BY title";
    $books = $Crud->db->query($sql_get_books)->fetchAll();
?>
<div>
    <div class="section">
        <h1 class="section-header">Books</h1>
        <div class="section-content">
            <table>
                <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Writer</th>
                    <th>Year</th>
                    <th>Action</th>
                </tr>
                <?php
                foreach ($books as $k => $v) {
                    ?>
                    <tr>
                        <td><?=++$k?></td>
                        <td><?=$v['title']?></td>
                        <td><?=$v['writer']?></td>
                        <td><?=$v['year']?></td>
                        <td>
                            <a class="btn btn-danger">Delete</a>
                            <a class="btn btn-warning">Edit</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>