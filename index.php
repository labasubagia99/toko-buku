<?php
    require_once __DIR__ . '/modules/crud.php';
    $Crud = new Crud;

    // ROUTES
    $router = array_diff(scandir(__DIR__."/pages/"), ['..', '.']);   
    
    // PAGES CONF
    $route = $_GET['r'] ?? 'home';
    $page_path = __DIR__."/pages/{$route}.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/css/index.css">
    <title>Toko Buku</title>
</head>
<body>
    <div class="container">
        <div class="sidebar">
            <h1 class="sidebar-header">Toko Buku</h1>
            <?php
            foreach ($router as $v) {
                $v = explode('.', $v)[0];
                echo("<a class='link " . ($route == $v ? 'active' : '') . "' href='index.php?r={$v}'>{$v}</a>");
            }
            ?>
        </div>
        <div class="content">
            <?php
            if (file_exists($page_path))
                require_once $page_path;
            ?>
        </div>
    </div>
</body>
</html>