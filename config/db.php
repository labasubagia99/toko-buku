<?php
    class Database {
        private $dbtype     = 'mysql';
        private $host       = 'localhost';
        private $username   = 'root';
        private $password   = 'root';
        private $dbname     = 'book_store';
        private $db_opt = [
            PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC
        ];
        public $db;

        public function __construct() {
            try {
                $this->db = new PDO("{$this->dbtype}:host={$this->host};dbname={$this->dbname}", $this->username, $this->password, $this->db_opt);
            } catch (PDOException $err) {
                echo $err->getMessage();
            }
        }
    }
?>